﻿using System;

namespace Average
{
    class Program
    {
        static void Main(string[] args)
        {
            double Number1,Number2,Number3,Number4;

            Console.Write("Enter a first number");
            Number1 = Convert.ToDouble(Console.ReadLine());

            Console.Write("Enter a second number");
            Number2 = Convert.ToDouble(Console.ReadLine());

            Console.Write("Enter a third number");
            Number3 = Convert.ToDouble(Console.ReadLine());

            Console.Write("Enter a fourth number");
            Number4 = Convert.ToDouble(Console.ReadLine());

            double result = (Number1 + Number2 + Number3 + Number4)/4;
            Console.WriteLine("The average of {0},{1},{2},{3} is {4}",Number1,Number2,Number3,Number4,result);
        }
    }
}
