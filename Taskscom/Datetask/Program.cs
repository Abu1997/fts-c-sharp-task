﻿using System;

namespace Datetask
{
    class DateMethod
    {
        static void Main(string[] args)
        {
            string YesorNo=null;
            do
            {

                Console.WriteLine("Task 1 : Date Format \n Task 2 : Date Format with seconds");
                Console.Write("Enter the number to Move to task :");
                int Num = Convert.ToInt32(Console.ReadLine());

                switch (Num)
                {
                    case 1:
                        {
                            DateTime date = new DateTime(2021, 2, 22, 13, 16, 20);
                            Console.WriteLine("The Complete time :" + date.ToString());

                            DateTime date1 = date.Date;

                            Console.WriteLine("The short time :" + date1.ToString("d"));

                            Console.WriteLine("Display date using 24-hour clock format:");

                            Console.WriteLine(date1.ToString("g"));
                            Console.WriteLine(date1.ToString("MM/dd/yyyy HH:mm"));
                        }
                        break;
                    case 2:
                        System.DateTime date2 = new System.DateTime(2021, 2, 22, 13, 25, 20, 11);

                        Console.WriteLine("year :" + date2.Year);
                        Console.WriteLine("month :" + date2.Month);
                        Console.WriteLine("date :" + date2.Date);
                        Console.WriteLine("hour :" + date2.Hour);
                        Console.WriteLine("minute :" + date2.Minute);
                        Console.WriteLine("second :" + date2.Second);
                        Console.WriteLine("millisecond :" + date2.Millisecond);
                        break;


                    default:
                        Console.WriteLine("(y/n) :");
                        break;
                }


                Console.WriteLine("");
                Console.WriteLine("Do you want continue (y/n) :");
                 YesorNo = Console.ReadLine();


            } while (YesorNo == "y");


            Console.WriteLine("The flow is Exit");

            // Console.CapsLock)dfdf
        }
    }

}
