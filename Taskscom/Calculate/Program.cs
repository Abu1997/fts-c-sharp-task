﻿using System;

namespace Tasks
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter a Number");
        int Num1 = Convert.ToInt32(Console.ReadLine());

        Console.Write("Enter a Another Number");
        int Num2 = Convert.ToInt32(Console.ReadLine());

        Console.WriteLine("{0} + {1} = {2}", Num1, Num2, Num1+Num2);
        Console.WriteLine("{0} - {1} = {2}", Num1, Num2, Num1 - Num2);
        Console.WriteLine("{0} * {1} = {2}", Num1, Num2, Num1 * Num2);
        Console.WriteLine("{0} / {1} = {2}", Num1, Num2, Num1 / Num2);
        Console.WriteLine("{0} % {1} = {2}", Num1, Num2, Num1 % Num2);
        }
    }
}
