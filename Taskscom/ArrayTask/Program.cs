﻿using System;

namespace ArrayTask
{
    class Program
    {
        static void Main(string[] args)
        {
           LoopAll();
        }

        public static void ComputeSum()
        {
             int[] number = {1,2,2,3,3,4,5,6,5,7,7,7,8,8,1};
            Console.WriteLine("Array : [{0}]",string.Join(",",number));

            var result = 0;

            for (int i =0; i<number.Length; i++)
            {
                result += number[i];

            }
            Console.WriteLine("Result : " +result);
        }
        public static void ComputeInteger()
        {
             int[] number = {1, 2, 5, 7, 8};
            Console.WriteLine("Array1 : [{0}]",string.Join(",",number));

            var result = number[0];

            for (int i =0; i<number.Length; i++)
            {
                if(number[i]>number[0])
                {
                    result = number[i];
                }

            }
            Console.WriteLine("Greatest number of value : " +result);
        }
        public static void TwoArray()
        {
             int i,j;
            int[,] Array = new int[3,3];

            Console.WriteLine("Matrix form of 3*3");

            Console.Write("Input the element in the matrix");
            for(i=0;i<3;i++){
                for(j=0;j<3;j++){
                    Console.Write("element is [{0},{1}] :",i,j);
                    Array[i,j]=Convert.ToInt32(Console.ReadLine());
                }
            }
            Console.Write("The matrix is : ");
            for(i=0;i<3;i++)
            {
              Console.Write("");
            for(j=0;j<3;j++)
           Console.Write("{0}\t",Array[i,j]);
            }
            Console.Write("");
        }
        public static void ArrayOdd()
        {
            int[] number1 = { 2, 4, 7, 8, 6 };
            

                for (int num =0; num<number1.Length;num++){
                    if (number1[num] % 2 != 0)
                    {
                        Console.WriteLine(true);
                        break;
                    } 
                }
        }
        public static void LoopAll()
        {
            Console.WriteLine("Task 1 : Compute the Sum");
            Console.WriteLine("Task 2 : Compute the Sum");
            Console.WriteLine("Task 3 : 2D Array");
            Console.WriteLine("Task 4 : Check Array Contains Odd Number");

            int User =Convert.ToInt32(Console.ReadLine());

            switch(User)
            {
                case 1: ComputeSum();
                break;
                case 2: ComputeInteger();
                break;
                case 3: TwoArray();
                break;
                case 4: ArrayOdd();
                break;
                default: Console.WriteLine("Exit the Method");
                break;
            }
            Console.WriteLine("\nDo you want to continue : ");
            string choose = Console.ReadLine();

             if (choose == "y")
            {
                LoopAll();
            }
            else
            {
                Console.WriteLine("The flow is exit");
            }
    
        }
    }
}
