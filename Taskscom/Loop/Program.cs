﻿using System;

namespace Loop
{
    class Program
    {
        static void Main(string[] args)
        {
            LoopAll();
        }

        public static void LoopNatural()
        {
            for (int i = 1; i <= 10; i++)
            {
                Console.WriteLine("The value is : {0}", i);
            }
        }
        public static void Sum()
        {
            int j, Sum = 0;
            Console.Write("The first 10 natural number are :");
            for (j = 1; j <= 10; j++)
            {
                Sum = Sum + j;
                Console.Write("{0} ", j);
            }
            Console.Write("The Sum is : {0}", Sum);
        }
        public static void Triangle()
        {
            int x;

            Console.Write("Number: ");
            x = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("{0}{0}{0}{0}", x);
            Console.WriteLine("{0} {0}", x);
            Console.WriteLine("{0} {0}", x);
            Console.WriteLine("{0} {0}", x);
            Console.WriteLine("{0}{0}{0}{0}", x);
        }
        public static void AngleTri()
        {
            Console.Write("number: ");
            int num = Convert.ToInt32(Console.ReadLine());

            Console.Write("width: ");
            int width = Convert.ToInt32(Console.ReadLine());

            int height = width;
            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    Console.Write(num);
                }

                Console.WriteLine();
                width--;
            }
        }
        public static void RightTriangle()
        {
            int i, j, rows;
            Console.Write("Input number of rows : ");
            rows = Convert.ToInt32(Console.ReadLine());
            for (i = 1; i <= rows; i++)
            {
                for (j = 1; j <= i; j++)
                    Console.Write("{0}", j);
                Console.Write("\n");
            }
        }
        public static void Pyramid()
        {
            int i, j, rows, spc, k, t = 0;
            Console.Write("Input number of rows : ");
            rows = Convert.ToInt32(Console.ReadLine());
            spc = rows + 4 - 1;
            for (i = 1; i <= rows; i++)
            {
                for (k = spc; k >= 1; k--)
                {
                    Console.Write(" ");
                }
                for (j = 1; j <= i; j++)
                    Console.Write("{0}", t++);
                Console.Write("\n");
                spc--;
            }
        }

        public static void LoopAll()
        {
            Console.WriteLine("Task 1 :  Loop 10 Numbers");
            Console.WriteLine("Task 2 :  Sum");
            Console.WriteLine("Task 3 :  Draw Rectangle");
            Console.WriteLine("Task 4 :  Displays a triangle");
            Console.WriteLine("Task 5 :  Right angle triangle");
            Console.WriteLine("Task 6 :  Pyramid");

            int User = Convert.ToInt32(Console.ReadLine());

            switch (User)
            {
                case 1:
                    LoopNatural();
                    break;
                case 2:
                    Sum();
                    break;
                case 3:
                    Triangle();
                    break;
                case 4:
                    AngleTri();
                    break;
                case 5:
                    RightTriangle();
                    break;
                case 6:
                    Pyramid();
                    break;

                default:
                    Console.WriteLine("Exit the Method");
                    break;
            }
            Console.WriteLine("\n Do you want to continue : ");
            string choose = Console.ReadLine();

            if (choose == "y")
            {
                LoopAll();
            }
            else
            {
                Console.WriteLine("The flow is exit");
           }
        }
    }
}
